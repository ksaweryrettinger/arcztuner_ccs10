#ifndef INCLUDES_PARAMETERS_ORIGINAL_H_
#define INCLUDES_PARAMETERS_ORIGINAL_H_

#include <limits.h>
#include "tm4c123gh6pm.h"

typedef enum {Large = 0, Small = 1} eTunerType;
typedef enum {Stopped = 0, Left = 1, Right = 2} eMotorDirection;

/* -------------------------------- RS-485 -------------------------------------*/

#define MAX_BYTES_UART_RX   (2)

/* --------------------------- Tuner Parameters --------------------------------*/

#define TUNER_ID             ('A')
#define TUNER_NUMBYTES       (7)
#define MOTOR_SPEED_TIMEOUT  (5) //seconds

/* -------------------------------- Globals ------------------------------------*/

//RS-485
extern volatile uint8_t ucBufferUART[MAX_BYTES_UART_RX]; // = { 0 };
extern volatile uint8_t ucByteNumUART; //=0
extern volatile uint8_t ucTunerButtonsTemp; //=0
extern volatile bool bTunerRx; // = true;
extern volatile bool bDataRequest; // = false;

//PWM
extern volatile float fPWMClock; // = 0;
extern volatile float fNewFrequency[2]; // = { 0 };
extern volatile float fCurrentFrequency[2]; // = { 0 };

//Motor speeds
extern volatile bool bLargeSpeedTimerRunning; // = false;
extern volatile bool bSmallSpeedTimerRunning; // = false;

//RS-485
extern volatile uint8_t ucTunerButtonsMaster; // = 0;
extern volatile uint8_t ucTunerDataMaster[TUNER_NUMBYTES]; // = { 0 };
extern volatile uint8_t ucTunerDataLocal[TUNER_NUMBYTES]; // = { 0 };
extern volatile bool bCriticalSectionLocker; // = false;

/* ---------------------------- Tuner Buttons ----------------------------------*/

#define BUTTON_AR    (0x01) //automatyczna (0) / ręczna (1)
#define BUTTON_ZS    (0x02) //zerowanie (0) / strojenie (1)
#define BUTTON_PZ    (0x04) //precyzyjny (0) / zgrubny (1)
#define BUTTON_PM_1  (0x08) //bit 0 of -/0/+ button
#define BUTTON_PM_2  (0x10) //bit 1 of -/0/+ button
#define BUTTON_OFF   (0x20) //offet button (- = 0, + = 1)

/* --------------------------- Tuner Data Bytes --------------------------------*/

#define DATA_TID      (0) //tuner ID: 'a' or 'b'
#define DATA_TLARGE0  (1) //large tuner position (Byte 0)
#define DATA_TLARGE1  (2) //large tuner position (Byte 1)
#define DATA_TSMALL0  (3) //small tuner position (Byte 0)
#define DATA_TSMALL1  (4) //small tuner position (Byte 1)
#define DATA_TMOTOR   (5) //motor movement information
#define DATA_TKRN     (6) //motor KRN+/- and RF status

/* ------------------------ Tuner Position Readings ----------------------------*/

#define MAX_CAPACITANCE  (15.0f)   //FDC1004 max capacitance
#define MAX_ADS1110      (0x7FFF)  //ADS1110 max output

/* -------------------------------- PWM ----------------------------------------*/

//TODO: confirm PWM resolution and rate of change
#define PWM_RESOLUTION  (0.05f) //smallest frequency change that can be detected
#define STEP_NUMCYCLES  (5) //number of clock cycles between frequency steps

/* ------------------------- Manual Motor Control ------------------------------*/

//TODO: confirm motor frequencies
#define FREQUENCY_STOPPED    (0)
#define MIN_FREQUENCY_LARGE  (30)
#define MID_FREQUENCY_LARGE  (50)
#define MAX_FREQUENCY_LARGE  (70)
#define MIN_FREQUENCY_SMALL  (70)
#define MID_FREQUENCY_SMALL  (110)
#define MAX_FREQUENCY_SMALL  (150)

/* ----------------------- Automatic Motor Control -----------------------------*/

//TODO: confirm tuner position and speed settings

//Small tuner positions
#define POS_SMALL_MIN       (20)
#define POS_SMALL_MAX       (80)
#define POS_SMALL_RETURN_L  (40) //control returned to small tuner at 40% (from 80%)
#define POS_SMALL_RETURN_H  (60) //control returned to small tuner at 60% (from 20%)

//Large tuner speeds
#define SPEED_LARGE_SLOW    (30) //Hz
#define SPEED_LARGE_MEDIUM  (50) //Hz
#define SPEED_LARGE_FAST    (70) //Hz
#define POS_LARGE_BOTTOM   (0) //% position
#define POS_LARGE_MIDDLE   (50) //% position
#define POS_LARGE_TOP      (70) //% position

//Temperature control
#define DTEMP_SMALL  (0.1)
#define DTEMP_LARGE  (0.2)

/* ----------------------- PID Controller Parameters ---------------------------*/

//TODO: optimize PID parameters
#define KP             (1.0f)
#define KI             (1.0f)
#define KD             (0)
#define EIMAX          (2000)
#define OFFSET_INIT    (2047)
#define PID_RESOLUTION (10) //number of PID calculations per second

/* -------------------------- I2C Configuration  -------------------------------*/

#define I2C_SPEED           (100000) //100kHz
#define I2C_MASTER_TIMEOUT  (12000)  //equivalent to 30 I2C clock cycles
#define I2C_MSG_NUMBYTES    (3)

/* ------------------------------ Addresses ------------------------------------*/

//TODO: confirm ADS1110 address
#define ADDRESS_1WM       (0b0011011) //DS2482-100
#define ADDRESS_EXPANDER  (0b0100000) //PCF8574
#define ADDRESS_ADS1110   (0b1001110) //ADS1110
#define ADDRESS_FDC1004   (0b1010000) //FDC1004

/* ------------------------ 1-Wire Communication -------------------------------*/

typedef enum eTempMode { DS2482100_Reset,
                         DS2482100_Configure,
                         DS18B20_StartConfigure,
                         DS18B20_Configure,
                         DS18B20_Reset,
                         DS18B20_StartConversion,
                         DS18B20_SensorBusy,
                         DS18B20_ConversionReady,
                         DS18B20_ReadTemperature,
                         Timeout,
                         ErrorSD,
                         ErrorI2C,
                         ConnectedI2C } eTempMode;

#define DS18B20_TIMEOUT  (2) //2s timeout
#define NUM_TEMP_BYTES   (2)

/* ------------------ 1-Wire Master Status Register Masks ---------------------*/

#define MASK_SD     (0x04)
#define MASK_1WB    (0x01)
#define MASK_RST    (0x10)
#define MASK_PPD    (0x02)
#define MASK_SBR    (0x20)

/* ------------------------ DS2482-100 (1WM) Commands -------------------------*/

#define WCFG        (0xD2)  //write configuration
#define WCFG_CMD    (0xE1)  //configuration byte
#define DRST        (0xF0)  //device reset (DS2482)
#define WRST        (0xB4)  //1-wire reset
#define WRBYTE      (0xA5)  //write single byte
#define RDBYTE      (0x96)  //read single byte
#define WBIT        (0x87)  //write single bit
#define RDTS        (0x80)  //read time slot
#define SETRP       (0xE1)  //set read pointer
#define SETRP_SR    (0xF0)  //set read pointer at status register
#define SETRP_RD    (0xE1)  //set read pointer at read data register

/* ------------------------ DS18B20 (Sensor) Commands -------------------------*/

#define SKPROM      (0xCC)
#define WRSCR       (0x4E)
#define RDSCR       (0xBE)
#define CONVT       (0x44)
#define REG_TL      (0x8A)  //TL = -10C
#define REG_TH      (0x64)  //TH = 100C
#define REG_CFG     (0x5F)  //11-bit resolution (R1 = 1, R0 = 0), 375ms conversion time

/* ------------------------- Pin Configuration ---------------------------------*/

/*
 * PE1 - Tiva ADC
 * PE4 - Large Motor Direction
 * PE5 - Small Motor Direction
 * PD1 - Small Motor PWM
 * PD3 - Large Motor PWM
 *
 * Expander Inputs:
 *
 * P0 - Large Tuner KRN-
 * P1 - Large Tuner KRN+
 * P2 - Small Tuner KRN-
 * P3 - Small Tuner KRN+
 * P4 - RF OK/NOK
 *
 */

/* -------------------- Macro Used to Hide Warnings ----------------------------*/

#define UNREFERENCED(x)  ((void)x)

#endif //INCLUDES_PARAMETERS_H_
