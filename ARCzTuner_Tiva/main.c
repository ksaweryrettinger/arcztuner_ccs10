// ARCz Tuner - TM4C123GH6PM
// Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/*---------------------------------- Includes ---------------------------------------------*/

#include "prototypes.h"
#include "parameters.h"

/* --------------------------------- Globals ----------------------------------------------*/

//Tuner data
volatile bool bCriticalSectionLocker = false;
volatile uint8_t ucTunerButtonsMaster = 0;
volatile uint8_t ucTunerDataMaster[TUNER_NUMBYTES] = { 0 };
volatile uint8_t ucTunerDataLocal[TUNER_NUMBYTES] = { 0 };

//Motor speed timeouts
volatile bool bLargeSpeedTimerRunning = false;
volatile bool bSmallSpeedTimerRunning = false;

/* --------------------------------- Static -----------------------------------------------*/

//PID controller
static uint32_t ulPIDError = 0;
static uint32_t ulPIDErrorPrevious = 0;
static uint32_t ulPIDErrorIntegral = 0;
static uint32_t ulPIDErrorDerivative = 0;
static float fTemperature = 0;
static float fTempPrevious = 0;
static float fTempDerivative = 0;
volatile static uint32_t ulPIDInput = 0;
volatile static uint32_t ulOffset = OFFSET_INIT;
volatile static float fPIDOutput = 0;

int main(void)
{
    /*---------------------------------------------- FPU ---------------------------------------------------*/

    FPULazyStackingEnable();
    FPUEnable();

    /*------------------------------------------- Local Data -----------------------------------------------*/

    //Counters
    uint8_t i;
    uint8_t j;

    //Buttons and tuner data
    uint8_t ucTunerButtons = 0;
    uint8_t ucTunerData[TUNER_NUMBYTES] = { 0 };

    //Expander
    uint8_t ucExpander = 0;
    bool bExpErrorI2C = false;

    //Tuner positions
    uint16_t usPosTunerLarge = 0;
    uint32_t ulPosTunerSmall = 0;
    uint32_t ulCtrlTunerSmall = 0;
    float fCapacitance = 0;
    float fCtrlCapacitance = 0;
    bool bADS1110ErrorI2C = false;
    bool bFDC1004ErrorI2C = false;

    //Tuner position conversion
    uint16_t usPosTunerLargeDec = 0;
    uint16_t usPosTunerSmallDec = 0;

    //PID variables
    uint32_t ulADCReading[4] = { 0 };
    float fPIDOutputLocal = 0;

    //Manutal motor control
    bool bLargeMotorRunning = false;
    bool bSmallMotorRunning = false;
    eMotorDirection eSmallMotorDirection = Stopped;
    eMotorDirection eLargeMotorDirection = Stopped;

    //Automatic motor control
    bool bSmallMotorControl = true;
    bool bLargeMotorControlLeft = false;
    bool bLargeMotorControlRight = false;
    bool bPIDTimerEnabled = false;
    bool bTempTimerEnabled = false;
    float fTempDerivativeLocal = 0;

    //DS18B20 communication
    eTempMode eMode = DS2482100_Reset;
    uint8_t ucTempReading[2] = { 0 };
    uint8_t ucMsgWrite[I2C_MSG_NUMBYTES] = { 0 };
    uint8_t ucMsgReceive[I2C_MSG_NUMBYTES] = { 0 };
    bool bTempErrorI2C = false;
    bool bTempErrorSD = false;
    bool bTempErrorTimeout = false;

    //Temperature readings
    uint16_t usTemperature = 0;

    //TODO: use debug variables in application logic or as debug outputs
    UNREFERENCED(bADS1110ErrorI2C);
    UNREFERENCED(bFDC1004ErrorI2C);
    UNREFERENCED(bExpErrorI2C);
    UNREFERENCED(bTempErrorI2C);
    UNREFERENCED(bTempErrorSD);
    UNREFERENCED(bTempErrorTimeout);

    //TODO: remove
    UNREFERENCED(fCtrlCapacitance);

    /*------------------------------------------- Tuner Data -----------------------------------------------*/

    if (TUNER_ID == 'A')
    {
        ucTunerData[DATA_TID] = 'a';
        ucTunerDataLocal[DATA_TID] = 'a';
        ucTunerDataMaster[DATA_TID] = 'a';
    }
    else if (TUNER_ID == 'B')
    {
        ucTunerData[DATA_TID] = 'b';
        ucTunerDataLocal[DATA_TID] = 'b';
        ucTunerDataMaster[DATA_TID] = 'b';
    }

    /*-------------------------------- System Clock Configuration (40MHz) ----------------------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------------------------ GPIO Peripherals ------------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    /*-------------------------------------------- GPIO Outputs --------------------------------------------*/

    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_4); //large motor direction
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_5); //small motor direction

    /*-------------------------------------- RS-485 UART Configuration -------------------------------------*/

    //TODO: configure additional RS-485 module to output debug characters

    //RS485 Rx/Tx GPIO
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_4);
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0); //Rx mode

    //Pin configuration
    GPIOPinConfigure(GPIO_PC4_U4RX);
    GPIOPinConfigure(GPIO_PC5_U4TX);
    GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);

    //Module configuration
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART4);
    UARTConfigSetExpClk(UART4_BASE, SysCtlClockGet(), 19200,
    (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_EVEN));
    UARTFIFODisable(UART4_BASE);
    IntEnable(INT_UART4);
    UARTIntEnable(UART4_BASE, UART_INT_RX | UART_INT_OE);

    /*---------------------------------------------- I2C Pins ----------------------------------------------*/

    //I2C-0 (DS2482-100, FDC1004, PCF8574))
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    //I2C-1 (ADS1110)
    GPIOPinConfigure(GPIO_PA6_I2C1SCL);
    GPIOPinConfigure(GPIO_PA7_I2C1SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTA_BASE, GPIO_PIN_6);
    GPIOPinTypeI2C(GPIO_PORTA_BASE, GPIO_PIN_7);

    /*------------------------------------------ I2C Configuration -----------------------------------------*/

    //Enable peripherals and set speed
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C1);
    I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false); //standard speed
    I2CMasterInitExpClk(I2C1_BASE, SysCtlClockGet(), false); //standard speed

    /*------------------------------------------ I2C Timeout Timer -----------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER1A);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

    /*---------------------------------------- FDC1004 Configuration ---------------------------------------*/

    //CONF_MEAS1 register (measurement configuration)
    ucMsgWrite[0] = 0x08; //register pointer
    ucMsgWrite[1] = 0x1C; //register setting (MSB)
    ucMsgWrite[2] = 0x00; //register setting (LSB)

    //Write to CONF_MEAS1
    if (bWriteI2C(I2C0_BASE, ucMsgWrite, 3, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
    else bFDC1004ErrorI2C = true;

    //CONF_MEAS2 register (measurement configuration)
    ucMsgWrite[0] = 0x09; //register pointer
    ucMsgWrite[1] = 0x3C; //register setting (MSB)
    ucMsgWrite[2] = 0x00; //register setting (LSB)

    //Write to CONF_MEAS2
    if (bWriteI2C(I2C0_BASE, ucMsgWrite, 3, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
    else bFDC1004ErrorI2C = true;

    //FDC_CONF register (measurement triggers)
    ucMsgWrite[0] = 0x0C; //register pointer
    ucMsgWrite[1] = 0x05; //register setting (MSB)
    ucMsgWrite[2] = 0xC0; //register setting (LSB)

    //Write to FDC_CONF (repeated measurements at 100S/s)
    if (bWriteI2C(I2C0_BASE, ucMsgWrite, 3, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
    else bFDC1004ErrorI2C = true;

    /*--------------------------------------------- Tiva ADC -----------------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1);
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0); //configure sample sequencer
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH2); //sample channel 2 (PE1)
    ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH2); //sample channel 2 (PE1)
    ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_CH2); //sample channel 2 (PE1)
    ADCSequenceStepConfigure(ADC0_BASE, 1, 3, ADC_CTL_CH2 | ADC_CTL_IE | ADC_CTL_END); //sample channel 2 (PE1)
    ADCHardwareOversampleConfigure(ADC0_BASE, 64); //64-sample hardware averaging
    ADCSequenceEnable(ADC0_BASE, 1); //enable sequencer

    /*--------------------------------------- PID Controller Timer -----------------------------------------*/

    //Wide Timer 0 (Periodic)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER0);
    TimerConfigure(WTIMER0_BASE, TIMER_CFG_A_PERIODIC);
    IntEnable(INT_WTIMER0A);
    TimerIntEnable(WTIMER0_BASE, TIMER_TIMA_TIMEOUT);
    TimerLoadSet64(WTIMER0_BASE, SysCtlClockGet() / PID_RESOLUTION);

    /*----------------------------------- Temperature Derivative Timer -------------------------------------*/

    //Wide Timer 1 (Periodic)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER1);
    TimerConfigure(WTIMER1_BASE, TIMER_CFG_A_PERIODIC);
    IntEnable(INT_WTIMER1A);
    TimerIntEnable(WTIMER1_BASE, TIMER_TIMA_TIMEOUT);
    TimerLoadSet64(WTIMER1_BASE, SysCtlClockGet());

    /*------------------------------------------- PWM Timers -----------------------------------------------*/

    //Large Tuner PWM (PD1, Wide Timer 2)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER2);
    GPIOPinConfigure(GPIO_PD1_WT2CCP1);
    GPIOPinTypeTimer(GPIO_PORTD_BASE, GPIO_PIN_1);
    TimerConfigure(WTIMER2_BASE, (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM));
    TimerControlLevel(WTIMER2_BASE, TIMER_B, false);
    TimerUpdateMode(WTIMER2_BASE, TIMER_B, TIMER_UP_LOAD_TIMEOUT | TIMER_UP_MATCH_TIMEOUT);

    //Small Tuner PWM (PD3, Wide Timer 3)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER3);
    GPIOPinConfigure(GPIO_PD3_WT3CCP1);
    GPIOPinTypeTimer(GPIO_PORTD_BASE, GPIO_PIN_3);
    TimerConfigure(WTIMER3_BASE, (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_B_PWM));
    TimerControlLevel(WTIMER3_BASE, TIMER_B, false);
    TimerUpdateMode(WTIMER3_BASE, TIMER_B, TIMER_UP_LOAD_TIMEOUT | TIMER_UP_MATCH_TIMEOUT);

    /*---------------------------------- Large Tuner Motor Speed Timer --------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    TimerConfigure(TIMER0_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    /*---------------------------------- Small Tuner Motor Speed Timer --------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER5);
    TimerConfigure(TIMER5_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER5A);
    TimerIntEnable(TIMER5_BASE, TIMER_TIMA_TIMEOUT);

    /*------------------------------------ PWM Frequency-Step Timers ----------------------------------------*/

    //Large Tuner
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
    TimerConfigure(TIMER2_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER2A);
    TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);

    //Small Tuner
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
    TimerConfigure(TIMER3_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER3A);
    TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);

    while (1)
    {
        /*------------------------------------------- Expander --------------------------------------------------*/

        if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 1, ADDRESS_EXPANDER))
        {
            ucExpander = ~(ucMsgReceive[0]);
            bExpErrorI2C = false;
        }
        else bExpErrorI2C = true;

        /*---------------------------------- Large Tuner Position (ADS1110) -------------------------------------*/

        if (bReceiveI2C(I2C1_BASE, ucMsgReceive, 3, ADDRESS_ADS1110))
        {
            if (!(ucMsgReceive[2] & 0x80)) //check for ST/DRDY = 0
            {
                if (!(ucMsgReceive[0] & 0x80)) //positive ADC values only
                {
                    //Store reading
                    usPosTunerLarge = (uint16_t) (ucMsgReceive[0] << 8);
                    usPosTunerLarge |= ucMsgReceive[1];
                }
            }

            bADS1110ErrorI2C = false;
        }
        else bADS1110ErrorI2C = true;

        /*---------------------------------- Small Tuner Position (FDC1004) -------------------------------------*/

        //Read FDC_CONF
        if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 2, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
        else bFDC1004ErrorI2C = true;

        //Check if measurements ready
        if (ucMsgReceive[1] & 0x0C)
        {
            //Set register pointer to MEAS1_MSB
            ucMsgWrite[0] = 0x00;
            if (bWriteI2C(I2C0_BASE, ucMsgWrite, 1, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
            else bFDC1004ErrorI2C = true;

            //Read MSB
            if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 2, ADDRESS_FDC1004))
            {
                ulPosTunerSmall = (uint32_t) ucMsgReceive[0] << 16; //bits 23-16
                ulPosTunerSmall |= (uint32_t) ucMsgReceive[1] << 8; //bits 15-8
                bFDC1004ErrorI2C = false;
            }
            else bFDC1004ErrorI2C = true;

            //Set register pointer to MEAS1_LSB
            ucMsgWrite[0] = 0x01;
            if (bWriteI2C(I2C0_BASE, ucMsgWrite, 1, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
            else bFDC1004ErrorI2C = true;

            //Read LSB
            if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 2, ADDRESS_FDC1004))
            {
                ulPosTunerSmall |= (uint32_t) ucMsgReceive[0]; //bits 7-0
                bFDC1004ErrorI2C = false;
            }
            else bFDC1004ErrorI2C = true;

            //Set register pointer to MEAS2_MSB
            ucMsgWrite[0] = 0x02;
            if (bWriteI2C(I2C0_BASE, ucMsgWrite, 1, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
            else bFDC1004ErrorI2C = true;

            //Read MSB
            if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 2, ADDRESS_FDC1004))
            {
               ulCtrlTunerSmall = (uint32_t) ucMsgReceive[0] << 16; //bits 23-16
               ulCtrlTunerSmall |= (uint32_t) ucMsgReceive[1] << 8; //bits 15-8
               bFDC1004ErrorI2C = false;
            }
            else bFDC1004ErrorI2C = true;

            //Set register pointer to MEAS2_LSB
            ucMsgWrite[0] = 0x03;
            if (bWriteI2C(I2C0_BASE, ucMsgWrite, 1, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
            else bFDC1004ErrorI2C = true;

            //Read LSB
            if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 2, ADDRESS_FDC1004))
            {
               ulCtrlTunerSmall |= (uint32_t) ucMsgReceive[0]; //bits 7-0
               bFDC1004ErrorI2C = false;
            }
            else bFDC1004ErrorI2C = true;

            //Calculate capacitances
            fCapacitance = (float)((~ulPosTunerSmall) + 1) / (float) 2E19;
            fCtrlCapacitance = (float)((~ulCtrlTunerSmall) + 1) / (float) 2E19;

            //Set register pointer to FDC_CONF
            ucMsgWrite[0] = 0x0C;
            if (bWriteI2C(I2C0_BASE, ucMsgWrite, 1, ADDRESS_FDC1004)) bFDC1004ErrorI2C = false;
            else bFDC1004ErrorI2C = true;
        }

        /*------------------------------------- Tuning Signal (Tiva ADC) ----------------------------------------*/

        //Get ADC reading
        ADCIntClear(ADC0_BASE, 1);
        ADCProcessorTrigger(ADC0_BASE, 1);
        while(!ADCIntStatus(ADC0_BASE, 1, false)) {}
        ADCSequenceDataGet(ADC0_BASE, 1, ulADCReading);
        ulPIDInput = (ulADCReading[0] + ulADCReading[1] + ulADCReading[2] + ulADCReading[3] + 2) / 4;

        /*-------------------------------------- Tuner Data Conversion ------------------------------------------*/

        //Large tuner position as 0-99%
        usPosTunerLargeDec = (((usPosTunerLarge * 99) + (MAX_ADS1110 - 1)) / MAX_ADS1110);
        if (usPosTunerLargeDec > 0) usPosTunerLargeDec -= 1;

        //Convert to ASCII
        ucTunerData[DATA_TLARGE0] = (usPosTunerLargeDec / 10) + '0';
        ucTunerData[DATA_TLARGE1] = (usPosTunerLargeDec % 10) + '0';

        //Small tuner position as 0-99%
        usPosTunerSmallDec = (uint8_t) round((fCapacitance * 99) / MAX_CAPACITANCE);
        if (usPosTunerSmallDec > 0) usPosTunerSmallDec -= 1;

        //Convert to ASCII
        ucTunerData[DATA_TSMALL0] = (usPosTunerSmallDec / 10) + '0';
        ucTunerData[DATA_TSMALL1] = (usPosTunerSmallDec % 10) + '0';

        //KRN and RF status
        ucTunerData[DATA_TKRN] = ucExpander;

        /*-------------------------------------- Manual Tuner Control -------------------------------------------*/

        if (ucTunerButtons & BUTTON_AR)
        {
            /*------------------------------------------ Motor Safety -----------------------------------------------*/

            //Large tuner
            if (bLargeMotorRunning && ((ucExpander & 0x01) || (ucExpander & 0x02))) //KRN+/- REACHED
            {
                //If not attempting to move in opposite direction
                if (!((ucExpander & 0x01) && eLargeMotorDirection == Right) ||
                     ((ucExpander & 0x02) && eLargeMotorDirection == Left))
                {
                    //Stop motor speed timer
                    if (bLargeSpeedTimerRunning)
                    {
                       TimerDisable(TIMER0_BASE, TIMER_BOTH);
                       bLargeSpeedTimerRunning = false;
                    }

                    //Stop motor
                    SetPWMFrequency(0, Large);
                    eLargeMotorDirection = Stopped;
                    bLargeMotorRunning = false;

                    //Update tuner data
                    ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                }
            }

            //Small tuner
            if (bSmallMotorRunning && ((ucExpander & 0x04) || (ucExpander & 0x08))) //KRN+/- REACHED
            {
                //If not attempting to move in opposite direction
                if (!((ucExpander & 0x04) && eSmallMotorDirection == Right) ||
                     ((ucExpander & 0x08) && eSmallMotorDirection == Left))
                {
                    //Stop motor speed timer
                    if (bSmallSpeedTimerRunning)
                    {
                       TimerDisable(TIMER5_BASE, TIMER_BOTH);
                       bSmallSpeedTimerRunning = false;
                    }

                    //Stop motor
                    SetPWMFrequency(0, Small);
                    eSmallMotorDirection = Stopped;
                    bSmallMotorRunning = false;

                    //Update tuner data
                    ucTunerData[DATA_TMOTOR] &= ~(0xF0);
                }
            }

            /*----------------------------------------- Tuner Movement ----------------------------------------------*/

            if (ucTunerButtons & BUTTON_PZ) //LARGE TUNER
            {
                if ((ucTunerButtons & BUTTON_PM_1) || (ucTunerButtons & BUTTON_PM_2)) //PRESSED +/-
                {
                    //Set motor direction
                    if ((ucTunerButtons & BUTTON_PM_1) && !(ucExpander & 0x01)) //LEFT (if not KRN-)
                    {
                        //Immediate direction change
                        if (eLargeMotorDirection == Right)
                        {
                            //Stop motor speed timer
                            if (bLargeSpeedTimerRunning)
                            {
                                TimerDisable(TIMER0_BASE, TIMER_BOTH);
                                bLargeSpeedTimerRunning = false;
                            }

                            //Stop motor
                            SetPWMFrequency(0, Large);
                            SysCtlDelay(1);

                            //Clear flag
                            bLargeMotorRunning = false;
                        }

                        //New direction
                        if (eLargeMotorDirection != Left)
                        {
                            //Set direction bit and update direction
                            GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, 0);
                            eLargeMotorDirection = Left;

                            //Update tuner data
                            ucTunerData[DATA_TMOTOR] |= 0x01;
                            ucTunerData[DATA_TMOTOR] &= ~(0x02 | 0x04 | 0x08);
                        }

                        //Start motor
                        if (!bLargeMotorRunning)
                        {
                            //Set new PWM frequency
                            SetPWMFrequency(MIN_FREQUENCY_LARGE, Large);

                            //Start motor speed timer
                            TimerLoadSet(TIMER0_BASE, TIMER_BOTH, SysCtlClockGet() * MOTOR_SPEED_TIMEOUT);
                            TimerEnable(TIMER0_BASE, TIMER_BOTH);

                            //Update flags
                            bLargeMotorRunning = true;
                            bLargeSpeedTimerRunning = true; //ISR flag
                        }
                    }
                    else if ((ucTunerButtons & BUTTON_PM_2) && !(ucExpander & 0x02)) //RIGHT (if not KRN+)
                    {
                        //Immediate direction change
                        if (eLargeMotorDirection == Left)
                        {
                            //Stop motor speed timer
                            if (bLargeSpeedTimerRunning)
                            {
                                TimerDisable(TIMER0_BASE, TIMER_BOTH);
                                bLargeSpeedTimerRunning = false;
                            }

                            //Stop motor
                            SetPWMFrequency(0, Large);
                            SysCtlDelay(1);

                            //Clear flag
                            bLargeMotorRunning = false;
                        }

                        //New direction
                        if (eLargeMotorDirection != Right)
                        {
                            //Set direction bit and update direction
                            GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, GPIO_PIN_4);
                            eLargeMotorDirection = Right;

                            //Update tuner data
                            ucTunerData[DATA_TMOTOR] |= 0x04;
                            ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x02 | 0x08);
                        }

                        //Start motor
                        if (!bLargeMotorRunning)
                        {
                            //Set new PWM frequency
                            SetPWMFrequency(MIN_FREQUENCY_LARGE, Large);

                            //Start motor speed timer
                            TimerLoadSet(TIMER0_BASE, TIMER_BOTH, SysCtlClockGet() * MOTOR_SPEED_TIMEOUT);
                            TimerEnable(TIMER0_BASE, TIMER_BOTH);

                            //Update flags
                            bLargeMotorRunning = true;
                            bLargeSpeedTimerRunning = true; //ISR flag
                        }
                    }

                    //Increase motor speed to 70Hz
                    if (bLargeMotorRunning && !bLargeSpeedTimerRunning)
                    {
                        //Set new PWM frequency
                        SetPWMFrequency(MAX_FREQUENCY_LARGE, Large);

                        //Set flag to prevent multiple calls
                        bLargeSpeedTimerRunning = true;

                        //Update tuner data
                        if (eLargeMotorDirection == Left)
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x02;
                            ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x04 | 0x08);
                        }
                        else if (eLargeMotorDirection == Right)
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x08;
                            ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x02 | 0x04);
                        }
                    }
                }
                else //RELEASED +/-
                {
                    //Stop motor speed timer
                    if (bLargeSpeedTimerRunning)
                    {
                        TimerDisable(TIMER0_BASE, TIMER_BOTH);
                        bLargeSpeedTimerRunning = false;
                    }

                    //Stop motor
                    SetPWMFrequency(0, Large);
                    eLargeMotorDirection = Stopped;
                    bLargeMotorRunning = false;

                    //Update tuner data
                    ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                }
            }
            else //SMALL TUNER
            {
                if ((ucTunerButtons & BUTTON_PM_1) || (ucTunerButtons & BUTTON_PM_2)) //PRESSED +/-
                {
                    //Set motor direction
                    if ((ucTunerButtons & BUTTON_PM_1) && !(ucExpander & 0x04)) //LEFT (if not KRN-)
                    {
                        //Immediate direction change
                        if (eSmallMotorDirection == Right)
                        {
                            //Stop motor speed timer
                            if (bSmallSpeedTimerRunning)
                            {
                                TimerDisable(TIMER5_BASE, TIMER_BOTH);
                                bSmallSpeedTimerRunning = false;
                            }

                            //Stop motor
                            SetPWMFrequency(0, Small);
                            SysCtlDelay(1);

                            //Clear flag
                            bSmallMotorRunning = false;
                        }

                        //New direction
                        if (eSmallMotorDirection != Left)
                        {
                            //Set direction bit and update direction
                            GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, 0);
                            eSmallMotorDirection = Left;

                            //Update tuner data
                            ucTunerData[DATA_TMOTOR] |= 0x10;
                            ucTunerData[DATA_TMOTOR] &= ~(0x20 | 0x40 | 0x80);
                        }

                        //Start motor
                        if (!bSmallMotorRunning)
                        {
                            SetPWMFrequency(MIN_FREQUENCY_SMALL, Small);

                            //Start motor speed timer
                            TimerLoadSet(TIMER5_BASE, TIMER_BOTH, SysCtlClockGet() * MOTOR_SPEED_TIMEOUT);
                            TimerEnable(TIMER5_BASE, TIMER_BOTH);

                            //Update flags
                            bSmallMotorRunning = true;
                            bSmallSpeedTimerRunning = true; //ISR flag
                        }
                    }
                    else if ((ucTunerButtons & BUTTON_PM_2) && !(ucExpander & 0x08)) //RIGHT (if not KRN+)
                    {
                        //Immediate direction change
                        if (eSmallMotorDirection == Left)
                        {
                            //Stop motor speed timer
                            if (bSmallSpeedTimerRunning)
                            {
                                TimerDisable(TIMER5_BASE, TIMER_BOTH);
                                bSmallSpeedTimerRunning = false;
                            }

                            //Stop motor
                            SetPWMFrequency(0, Small);
                            SysCtlDelay(1);

                            //Clear flag
                            bSmallMotorRunning = false;
                        }

                        //New direction
                        if (eSmallMotorDirection != Right)
                        {
                            //Set direction bit and update direction
                            GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, GPIO_PIN_5);
                            eSmallMotorDirection = Right;

                            //Update tuner data
                            ucTunerData[DATA_TMOTOR] |= 0x40;
                            ucTunerData[DATA_TMOTOR] &= ~(0x10 | 0x20 | 0x80);
                        }

                        //Start motor
                        if (!bSmallMotorRunning)
                        {
                            //Set new PWM frequency
                            SetPWMFrequency(MIN_FREQUENCY_SMALL, Small);

                            //Start motor speed timer
                            TimerLoadSet(TIMER5_BASE, TIMER_BOTH, SysCtlClockGet() * MOTOR_SPEED_TIMEOUT);
                            TimerEnable(TIMER5_BASE, TIMER_BOTH);

                            //Update flags
                            bSmallMotorRunning = true;
                            bSmallSpeedTimerRunning = true; //ISR flag
                        }
                    }

                    //Increase motor speed to 150Hz
                    if (bSmallMotorRunning && !bSmallSpeedTimerRunning)
                    {
                        //Set new PWM frequency
                        SetPWMFrequency(MAX_FREQUENCY_SMALL, Small);

                        //Set flag to prevent multiple calls
                        bSmallSpeedTimerRunning = true;

                        //Update tuner data
                        if (eSmallMotorDirection == Left)
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x20;
                            ucTunerData[DATA_TMOTOR] &= ~(0x10 | 0x40 | 0x80);
                        }
                        else if (eSmallMotorDirection == Right)
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x80;
                            ucTunerData[DATA_TMOTOR] &= ~(0x10 | 0x20 | 0x40);
                        }
                    }
                }
                else //RELEASED +/-
                {
                    //Stop motor speed timer
                    if (bSmallSpeedTimerRunning)
                    {
                        TimerDisable(TIMER5_BASE, TIMER_BOTH);
                        bSmallSpeedTimerRunning = false;
                    }

                    SetPWMFrequency(0, Small);
                    eSmallMotorDirection = Stopped;
                    bSmallMotorRunning = false;

                    //Update tuner data
                    ucTunerData[DATA_TMOTOR] &= ~(0xF0);
                }
            }
        }

        /*------------------------------------ Automatic Tuner Control ------------------------------------------*/

        if (!(ucTunerButtons & BUTTON_AR))
        {
            //Offset correction
            if (ucTunerButtons & BUTTON_ZS)
            {
                if (!(ucTunerButtons & BUTTON_OFF)) //offset -
                {
                    //TODO: offset correction (-), use PID timer to periodically update value?
                    //ulOffset -= ...
                }
                else if (ucTunerButtons & BUTTON_OFF) //offset +
                {
                    //TODO: offset correction (+), use PID timer to periodically update value?
                    //ulOffset += ...
                }
            }

            //PID Controller
            if (ucTunerData[DATA_TKRN] & 0x10) //RF OK
            {
                //Disable temperature derivative timer
                if (bTempTimerEnabled)
                {
                    TimerDisable(WTIMER1_BASE, TIMER_A);
                    bTempTimerEnabled = false;
                }

                //Check control level (small/large tuner) based on small tuner position
                if (usPosTunerSmallDec >= MIN_POSITION_SMALL && usPosTunerSmallDec <= MAX_POSITION_SMALL && bSmallMotorControl)
                {
                    if ((ucExpander & 0x04) || (ucExpander & 0x08)) //small tuner KRN+/- reached
                    {
                        //Stop small tuner
                        SetPWMFrequency(0, Small);
                        ucTunerData[DATA_TMOTOR] &= ~(0xF0);

                        //Ensure large tuner is stopped
                        SetPWMFrequency(0, Large);
                        ucTunerData[DATA_TMOTOR] &= ~(0x0F);

                        //Switch off PID timer
                        if (bPIDTimerEnabled)
                        {
                            TimerDisable(WTIMER0_BASE, TIMER_A);
                            bPIDTimerEnabled = false;
                        }
                    }
                    else //normal operation
                    {
                        //Switch on PID timer
                        if (!bPIDTimerEnabled)
                        {
                           TimerEnable(WTIMER0_BASE, TIMER_A);
                           bPIDTimerEnabled = true;

                           //Stop large tuner
                           SetPWMFrequency(0, Large);
                           ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                        }

                        /*----------------------------------------- PID Control ----------------------------------------------*/

                        //Critical section - disable interrupts
                        IntDisable(INT_WTIMER0A);
                        fPIDOutputLocal = fPIDOutput;
                        IntEnable(INT_WTIMER0A);

                        //Limit PID output
                        if (fPIDOutputLocal > MAX_FREQUENCY_SMALL) fPIDOutputLocal = MAX_FREQUENCY_SMALL;
                        else if (fPIDOutputLocal < (-1)*MAX_FREQUENCY_SMALL) fPIDOutputLocal = (-1)*MAX_FREQUENCY_SMALL;

                        //Set direction
                        if (fPIDOutputLocal > 0) //moving LEFT to decrease PID output
                        {
                           //Change direction bit
                           GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, 0);

                           //Update tuner data
                           if (fPIDOutputLocal >= MID_FREQUENCY_SMALL) //FAST LEFT
                           {
                               ucTunerData[DATA_TMOTOR] |= 0x20;
                               ucTunerData[DATA_TMOTOR] &= ~(0x10 | 0x40 | 0x80);
                           }
                           else //NORMAL SPEED LEFT
                           {
                               ucTunerData[DATA_TMOTOR] |= 0x10;
                               ucTunerData[DATA_TMOTOR] &= ~(0x20 | 0x40 | 0x80);
                           }
                        }
                        else if (fPIDOutputLocal < 0) //moving RIGHT to increase PID output
                        {
                           //Change direction bit
                           GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_5, GPIO_PIN_5);

                           //Update tuner data
                           if (fPIDOutputLocal >= MID_FREQUENCY_SMALL) //FAST RIGHT
                           {
                               ucTunerData[DATA_TMOTOR] |= 0x80;
                               ucTunerData[DATA_TMOTOR] &= ~(0x10 | 0x20 | 0x40);
                           }
                           else //NORMAL SPEED RIGHT
                           {
                               ucTunerData[DATA_TMOTOR] |= 0x40;
                               ucTunerData[DATA_TMOTOR] &= ~(0x10 | 0x20 | 0x80);
                           }
                        }

                        //Update small motor PWM
                        SetPWMFrequency(fabs(fPIDOutputLocal), Small);
                    }
                }
                else if (usPosTunerSmallDec > MAX_POSITION_SMALL && !bLargeMotorControlLeft) //large tuner moving LEFT (correction)
                {
                    //Set flags
                    bSmallMotorControl = false;
                    bLargeMotorControlLeft = true;

                    //Stop small tuner
                    SetPWMFrequency(0, Small);
                    ucTunerData[DATA_TMOTOR] &= ~(0xF0);

                    //Set large tuner direction
                    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, 0); //moving left

                    //Switch off PID timer
                    if (bPIDTimerEnabled)
                    {
                        TimerDisable(WTIMER0_BASE, TIMER_A);
                        bPIDTimerEnabled = false;
                    }
                }
                else if (usPosTunerSmallDec < MIN_POSITION_SMALL && !bLargeMotorControlRight) //large tuner moving RIGHT (correction)
                {
                    //Set flags
                    bSmallMotorControl = false;
                    bLargeMotorControlRight = true;

                    //Stop small tuner
                    SetPWMFrequency(0, Small);
                    ucTunerData[DATA_TMOTOR] &= ~(0xF0);

                    //Set large tuner direction
                    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, GPIO_PIN_4); //moving right

                    //Switch off PID timer
                    if (bPIDTimerEnabled)
                    {
                        TimerDisable(WTIMER0_BASE, TIMER_A);
                        bPIDTimerEnabled = false;
                    }
                }

                /*------------------------------------- Large Tuner Control ------------------------------------------*/

                if (bLargeMotorControlLeft) //large tuner moving LEFT (correction)
                {
                    if (ucExpander & 0x01) //large tuner KRN- reached
                    {
                        //Stop large tuner
                        SetPWMFrequency(0, Large);
                        ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                    }
                    else if (usPosTunerSmallDec > POS_SMALL_RETURN_L)
                    {
                        //Set large motor speed based on large tuner position
                        if (usPosTunerLargeDec >= POS_LARGE_TOP) SetPWMFrequency(SPEED_LARGE_FAST, Large); //fast speed
                        else if (usPosTunerLargeDec >= POS_LARGE_MEDIUM) SetPWMFrequency(SPEED_LARGE_MEDIUM, Large); //medium speed
                        else if (usPosTunerLargeDec >= POS_LARGE_BOTTOM) SetPWMFrequency(SPEED_LARGE_SLOW, Large); //slow speed

                        //Update tuner data
                        if (usPosTunerLargeDec >= POS_LARGE_MEDIUM) //FAST LEFT
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x02;
                            ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x04 | 0x08);
                        }
                        else //NORMAL SPEED LEFT
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x01;
                            ucTunerData[DATA_TMOTOR] &= ~(0x02 | 0x04 | 0x08);
                        }
                    }
                    else
                    {
                        //Return control to small tuner
                        bLargeMotorControlLeft = false;
                        bSmallMotorControl = true;

                        //Stop large tuner
                        SetPWMFrequency(0, Large);
                        ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                    }
                }
                else if (bLargeMotorControlRight) //large tuner moving RIGHT (correction)
                {
                    if (ucExpander & 0x02) //large tuner KRN+ reached
                    {
                        //Stop large tuner
                        SetPWMFrequency(0, Large);
                        ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                    }
                    else if (usPosTunerSmallDec < POS_SMALL_RETURN_H)
                    {
                        //Set large motor speed based on large tuner position
                        if (usPosTunerLargeDec <= POS_LARGE_MEDIUM) SetPWMFrequency(SPEED_LARGE_FAST, Large);
                        else if (usPosTunerLargeDec <= POS_LARGE_TOP) SetPWMFrequency(SPEED_LARGE_MEDIUM, Large);
                        else SetPWMFrequency(SPEED_LARGE_SLOW, Large);

                        //Update tuner data
                        if (usPosTunerLargeDec <= POS_LARGE_TOP) //FAST RIGHT
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x08;
                            ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x02 | 0x04);
                        }
                        else //NORMAL SPEED RIGHT
                        {
                            ucTunerData[DATA_TMOTOR] |= 0x04;
                            ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x02 | 0x08);
                        }
                    }
                    else
                    {
                        //Return control to small tuner
                        bLargeMotorControlRight = false;
                        bSmallMotorControl = true;

                        //Stop large tuner
                        SetPWMFrequency(0, Large);
                        ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                    }
                }
            }
            else //RF NOK
            {
                //Switch off PID timer and small tuner motor
                if (bPIDTimerEnabled)
                {
                    //Stop timer
                    TimerDisable(WTIMER0_BASE, TIMER_A);
                    bPIDTimerEnabled = false;

                    //Stop small tuner
                    SetPWMFrequency(0, Small);
                    ucTunerData[DATA_TMOTOR] &= ~(0xF0);
                }

                if ((ucExpander & 0x01) || (ucExpander & 0x02)) //large tuner KRN+/- reached
                {
                    //Stop large tuner
                    SetPWMFrequency(0, Large);
                    ucTunerData[DATA_TMOTOR] &= ~(0x0F);
                }
                else
                {
                    //Enable temperature derivative timer
                    if (!bTempTimerEnabled)
                    {
                        TimerEnable(WTIMER1_BASE, TIMER_A);
                        bTempTimerEnabled = true;
                    }

                    //Critical section - disable interrupts
                    IntDisable(INT_WTIMER1A);
                    fTempDerivativeLocal = fTempDerivative;
                    IntEnable(INT_WTIMER1A);

                    //Large tuner control
                    if (fTempDerivativeLocal > DTEMP_LARGE) //moving fast LEFT
                    {
                        //Set direction
                        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, 0);

                        //Set speed
                        SetPWMFrequency(SPEED_LARGE_FAST, Large);

                        //Update tuner data
                        ucTunerData[DATA_TMOTOR] |= 0x02;
                        ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x04 | 0x08);
                    }
                    else if (fTempDerivativeLocal > DTEMP_SMALL) //moving normal LEFT
                    {
                        //Set direction
                        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, 0);

                        //Set speed
                        SetPWMFrequency(SPEED_LARGE_SLOW, Large);

                        //Update tuner data
                        ucTunerData[DATA_TMOTOR] |= 0x01;
                        ucTunerData[DATA_TMOTOR] &= ~(0x02 | 0x04 | 0x08);
                    }
                    else if (fTempDerivativeLocal < (-1)*DTEMP_LARGE) //moving fast RIGHT
                    {
                        //Set direction
                        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, GPIO_PIN_4);

                        //Set speed
                        SetPWMFrequency(SPEED_LARGE_FAST, Large);

                        //Update tuner data
                        ucTunerData[DATA_TMOTOR] |= 0x08;
                        ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x02 | 0x04);
                    }
                    else if (fTempDerivativeLocal < (-1)*DTEMP_SMALL) //moving normal RIGHT
                    {
                        //Set direction
                        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, GPIO_PIN_4);

                        //Set speed
                        SetPWMFrequency(SPEED_LARGE_SLOW, Large);

                        //Update tuner data
                        ucTunerData[DATA_TMOTOR] |= 0x04;
                        ucTunerData[DATA_TMOTOR] &= ~(0x01 | 0x02 | 0x08);
                    }
                }
            }
        }

        /*---------------------------------------- Temperature Sensor -------------------------------------------*/

        switch (eMode)
        {
            case DS2482100_Reset: //Send Reset Command to DS2482-100

                //Reset 1WM
                ucMsgWrite[0] = DRST;
                ucMsgWrite[1] = NULL;
                if (!bWriteCommand1WM(1, ucMsgWrite, &eMode)) break;

                eMode = DS2482100_Configure;
                break;

            case DS2482100_Configure: //Setup DS2482-100 Configuration Register

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Wait for 1WB = 0 and RST = 1
                if (((ucMsgReceive[0] & MASK_1WB) == NULL) && ((ucMsgReceive[0] & MASK_RST) != NULL))
                {
                    //Set 1WM configuration register
                    ucMsgWrite[0] = WCFG;
                    ucMsgWrite[1] = WCFG_CMD;
                    if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;

                    //Set RP at status register
                    ucMsgWrite[0] = SETRP;
                    ucMsgWrite[1] = SETRP_SR;
                    if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;

                    //Switch mode to sensor initialization
                    eMode = DS18B20_StartConfigure;
                    break;
                }

                break;

            case DS18B20_StartConfigure: //Start DS18B20 Configuration

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Wait for 1WB = 0
                if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                {
                    //Initiate 1-Wire Reset
                    ucMsgWrite[0] = WRST;
                    ucMsgWrite[1] = NULL;
                    if (!bWriteCommand1WM(1, ucMsgWrite, &eMode)) break;

                    //Switch to waiting mode
                    eMode = DS18B20_Configure;
                }

                break;

            case DS18B20_Configure: //Complete DS18B20 Configuration

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Wait for 1WB = 0 and PPD = 1
                if (((ucMsgReceive[0] & MASK_1WB) == NULL) && ((ucMsgReceive[0] & MASK_PPD) != NULL))
                {
                    //Contruct next message
                    if ((ucMsgReceive[0] & MASK_PPD) && ucMsgWrite[1] == NULL)
                    {
                        //DS18B20 Skip-Rom
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = SKPROM;
                    }
                    else if (ucMsgWrite[1] == SKPROM)
                    {
                        //DS18B20 Write to Scratchpad
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = WRSCR;
                    }
                    else if (ucMsgWrite[1] == WRSCR)
                    {
                        //DS18B20 TL Register
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = REG_TL; //TL = -10C
                    }
                    else if (ucMsgWrite[1] == REG_TL)
                    {
                        //DS18B20 TH Register
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = REG_TH; //TH = 100C
                    }
                    else if (ucMsgWrite[1] == REG_TH)
                    {
                        //DS18B20 Configuration Register
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = REG_CFG; //11-bit, 375ms conversion time
                    }
                    else if (ucMsgWrite[1] == REG_CFG)
                    {
                        //Clear write register
                        ucMsgWrite[0] = NULL;
                        ucMsgWrite[1] = NULL;

                        //Sensor initialization complete, switch to 'Reset' mode
                        eMode = DS18B20_Reset;

                        break;
                    }

                    //Send command to 1WM
                    if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;
                }

                break;

            case DS18B20_Reset: //Reset DS18B20

                //Set RP at status register
                ucMsgWrite[0] = SETRP;
                ucMsgWrite[1] = SETRP_SR;
                if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Wait for 1WB = 0
                if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                {
                    //Initiate 1-Wire Reset
                    ucMsgWrite[0] = WRST;
                    ucMsgWrite[1] = NULL;
                    if (!bWriteCommand1WM(1, ucMsgWrite, &eMode)) break;

                    //Switch to waiting mode
                    eMode = DS18B20_StartConversion;
                }

                break;

            case DS18B20_StartConversion: //Start DS18B20 Temperature Conversion

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Wait for 1WB = 0 and PPD = 1
                if (((ucMsgReceive[0] & MASK_1WB) == NULL) && ((ucMsgReceive[0] & MASK_PPD) != NULL))
                {
                    if (ucMsgWrite[1] == NULL)
                    {
                        //DS18B20 Skip-Rom
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = SKPROM;
                        bWriteCommand1WM(2, ucMsgWrite, &eMode);
                        break;
                    }
                    else if (ucMsgWrite[1] == SKPROM)
                    {
                        //DS18B20 Convert T
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = CONVT;
                        if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;

                        //Switch to waiting for temperature conversion (slave busy)
                        eMode = DS18B20_SensorBusy;
                    }
                }
                else break;

            case DS18B20_SensorBusy: //Wait for DS18B20 Temperature Conversion

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Check for data ready
                if (((ucMsgReceive[0] & MASK_1WB) == NULL) && (ucMsgWrite[1] != RDTS))  //Send DS18B20 Query
                {
                    //Issue Read Time Slot
                    ucMsgWrite[0] = WBIT;
                    ucMsgWrite[1] = RDTS;
                    bWriteCommand1WM(2, ucMsgWrite, &eMode);
                    break;
                }
                else if (((ucMsgReceive[0] & MASK_1WB) == NULL) && (ucMsgWrite[1] == RDTS)) //DS18B20 Query Sent
                {
                    //Clear write register
                    ucMsgWrite[0] = NULL;
                    ucMsgWrite[1] = NULL;

                    //Wait for SBR = 1
                    if ((ucMsgReceive[0] & MASK_SBR) != NULL)
                    {
                        //Initiate 1-Wire Reset
                        ucMsgWrite[0] = WRST;
                        ucMsgWrite[1] = NULL;
                        if (!bWriteCommand1WM(1, ucMsgWrite, &eMode)) break;

                        //Read DS18B20 Scratchpad
                        eMode = DS18B20_ConversionReady;
                    }
                }
                else break;

            case DS18B20_ConversionReady: //DS18B20 Temperature Conversion Ready, Read Scratchpad

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Wait for 1WB = 0
                if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                {
                    if (ucMsgWrite[1] == NULL)
                    {
                        //DS18B20 Skip-Rom
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = SKPROM;
                        bWriteCommand1WM(2, ucMsgWrite, &eMode);
                        break;
                    }
                    else if (ucMsgWrite[1] == SKPROM)
                    {
                        //DS18B20 Read Scratchpad
                        ucMsgWrite[0] = WRBYTE;
                        ucMsgWrite[1] = RDSCR;
                        if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;

                        //Switch to waiting for temperature conversion (slave busy)
                        eMode = DS18B20_ReadTemperature;
                    }
                }
                else break;

            case DS18B20_ReadTemperature: //Read DS18B20 Temperature

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Wait for 1WB = 0
                if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                {
                    for (j = 0; j < NUM_TEMP_BYTES; j++)
                    {
                        //Read Single Byte
                        ucMsgWrite[0] = RDBYTE;
                        ucMsgWrite[1] = NULL;
                        if (!bWriteCommand1WM(1, ucMsgWrite, &eMode)) break;

                        //Wait for 1WB = 0
                        while (1)
                        {
                            //Read 1WM Status Register
                            if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;
                            if (((ucMsgReceive[0] & MASK_1WB) == NULL)) break;
                        }

                        //Set RP at Read Data Register
                        ucMsgWrite[0] = SETRP;
                        ucMsgWrite[1] = SETRP_RD;
                        if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;

                        /*-------------------------------- Read Temperature Data ------------------------------*/

                        if (!bReadData1WM(&ucTempReading[j], &eMode)) break;

                        /*-------------------------------------------------------------------------------------*/

                        //Set RP at status register
                        ucMsgWrite[0] = SETRP;
                        ucMsgWrite[1] = SETRP_SR;
                        if (!bWriteCommand1WM(2, ucMsgWrite, &eMode)) break;

                        //Wait for 1WB = 0
                        while (1)
                        {
                            //Read 1WM Status Register
                            if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;
                            if (((ucMsgReceive[0] & MASK_1WB) == NULL)) break;
                        }

                        //Check for mode changes and errors
                        if (eMode != DS18B20_ReadTemperature) break;

                        //Reading temperature complete
                        if (j == (NUM_TEMP_BYTES - 1)) eMode = DS18B20_Reset;
                    }
                }

                //Store new temperature reading
                if (eMode == DS18B20_Reset)
                {
                    usTemperature = ucTempReading[0];
                    usTemperature |= ucTempReading[1] << 8;

                    //Convert to degrees C (debug only)
                    if (usTemperature & 0x8000)
                    {
                        //Negative temperature - get 2's complement
                        usTemperature = (usTemperature ^ 0xFFFF) + 1;

                        fTemperature = (-1) * ((usTemperature >> 4) + (((usTemperature & 0x8) >> 3) * 0.5f) +
                                (((usTemperature & 0x4) >> 2) * 0.25f) + (((usTemperature & 0x2) >> 1) * 0.125f));
                    }
                    else
                    {
                        fTemperature = ((usTemperature >> 4) + (((usTemperature & 0x8) >> 3) * 0.5f) +
                                (((usTemperature & 0x4) >> 2) * 0.25f) + (((usTemperature & 0x2) >> 1) * 0.125f));
                    }
                }

                break;

            case ErrorSD: //DS2482-100 Short Detected

                //Clear write register
                ucMsgWrite[0] = NULL;
                ucMsgWrite[1] = NULL;

                //Update Modbus data
                bTempErrorSD = true;

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //SD = 0, clear errors and switch to reset mode
                if ((ucMsgReceive[0] & MASK_SD) == NULL)
                {
                    bTempErrorSD = false;
                    eMode = DS18B20_Reset;
                }

                break;

            case ErrorI2C: //I2C Communication Errors

                //Clear write register
                ucMsgWrite[0] = NULL;
                ucMsgWrite[1] = NULL;

                //Update Modbus data
                bTempErrorI2C = true;

                //Attempt Reading 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //Read successful, clear errors and switch to Reset mode
                bTempErrorI2C = false;
                eMode = DS18B20_Reset;

                break;

            case Timeout:

                //Update local and Modbus data
                fTemperature = 0;
                usTemperature = 0;
                bTempErrorTimeout = true;

                (void) bReadStatus1WM(ucMsgReceive, &eMode);

                //Attempt 1-Wire Reset
                if ((ucMsgReceive[0] & MASK_1WB) == NULL)
                {
                    ucMsgWrite[0] = WRST;
                    ucMsgWrite[1] = NULL;
                    if (!bWriteCommand1WM(1, ucMsgWrite, &eMode)) break;
                }

                //Read 1WM Status Register
                if (!bReadStatus1WM(ucMsgReceive, &eMode)) break;

                //1WB = 0, clear errors and switch to reset mode
                bTempErrorTimeout = false;
                eMode = DS18B20_Reset;
        }

        /*--------------------------------------- RS-485 Data Update --------------------------------------------*/

        //Update tuner data
        if (!bCriticalSectionLocker)
        {
            bCriticalSectionLocker = true;
            IntDisable(INT_UART4);
            for (i = 0; i < TUNER_NUMBYTES; i++) ucTunerDataMaster[i] = ucTunerData[i];
            IntEnable(INT_UART4);
            bCriticalSectionLocker = false;
        }

        //Update button configuration
        if (!bCriticalSectionLocker) ucTunerButtons = ucTunerButtonsMaster;
    }
}

//PID calculation timer ISR (/100ms)
void IntHandlerTimerPID(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(WTIMER0_BASE, true);
    TimerIntClear(WTIMER0_BASE, ui32status);

    //TODO: double check equations and whether error integral limit is necessary

    //Calculate error
    ulPIDError = ulOffset - ulPIDInput;

    //Calculate error intergral
    ulPIDErrorIntegral += ulPIDError;

    //Limit error integral (avoid integral windup)
    if (ulPIDErrorIntegral > EIMAX ) ulPIDErrorIntegral = EIMAX;
    else if (ulPIDErrorIntegral < (-1)*EIMAX) ulPIDErrorIntegral = (-1)*EIMAX;

    //Calculate error derivative
    ulPIDErrorDerivative = ulPIDError - ulPIDErrorPrevious;
    ulPIDErrorPrevious = ulPIDError;

    //Calculate PID output
    fPIDOutput = KP*ulPIDError + KI*ulPIDErrorIntegral + KD*ulPIDErrorDerivative;
}

//Temperature derivative calculation timer ISR (/s)
void IntHandlerTimerTempDiff(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(WTIMER1_BASE, true);
    TimerIntClear(WTIMER1_BASE, ui32status);

    //Calculate temperature derivative
    fTempDerivative = fTemperature - fTempPrevious;
    fTempPrevious = fTemperature;
}
