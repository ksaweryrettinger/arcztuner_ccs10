#include "parameters.h"
#include "prototypes.h"

volatile bool bTimeoutDS18B20 = false;

bool bWriteCommand1WM(uint8_t ucByteNum, uint8_t* ucMsgWrite, eTempMode* eMode)
{
    if (!bWriteI2C(I2C0_BASE, ucMsgWrite, ucByteNum, ADDRESS_1WM))
    {
        *eMode = ErrorI2C;
        return false;
    }
    else
    {
        //Start timeout timer
        TimerLoadSet(TIMER4_BASE, TIMER_BOTH, SysCtlClockGet() * DS18B20_TIMEOUT);
        TimerEnable(TIMER4_BASE, TIMER_BOTH);
    }

    return true;
}

bool bReadStatus1WM(uint8_t* ucMsgReceive, eTempMode* eMode)
{
    //Read 1WM Status Register and check for I2C errors
    if (!bReceiveI2C(I2C0_BASE, ucMsgReceive, 1, ADDRESS_1WM))
    {
        *eMode = ErrorI2C;
        return false;
    }

    //Check for SD errors
    if ((ucMsgReceive[0] & MASK_SD))
    {
        if (*eMode != ErrorSD) *eMode = ErrorSD; //short detected
        return false;
    }

    //Check for 1WM reset events
    if (ucMsgReceive[0] & MASK_RST)
    {
        if (*eMode != DS2482100_Configure)
        {
            *eMode = DS2482100_Configure; //1WM reset
            return false;
        }
    }

    //Check for DS18B20 timeout errors
    if (bTimeoutDS18B20)
    {
        if ((ucMsgReceive[0] & MASK_1WB) == NULL && (ucMsgReceive[0] & MASK_PPD) != NULL) //1WB = 0, PPD = 1
        {
            bTimeoutDS18B20 = false;
        }
        else
        {
            if (*eMode != Timeout) *eMode = Timeout;
            return false;
        }
    }
    else if ((ucMsgReceive[0] & MASK_1WB) == NULL && (ucMsgReceive[0] & MASK_PPD) != NULL) //1WB = 0, PPD = 1
    {
        //Presence pulse detected, 1-Wire ready, clear timeout
        bTimeoutDS18B20 = false;
        TimerDisable(TIMER4_BASE, TIMER_BOTH);
    }

    //No errors
    return true;
}

bool bReadData1WM(uint8_t* ucMsgReceive, eTempMode* eMode)
{
    //Read 1WM Status Register and check for I2C errors
    if (!bReceiveI2C(I2C0_BASE, ucMsgReceive, 1, ADDRESS_1WM))
    {
        *eMode = ErrorI2C;
        return false;
    }

    //No errors - return mode setting
    return true;
}

void IntHandlerTimerDS18B20(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER4_BASE, true);
    TimerIntClear(TIMER4_BASE, ui32status);
    bTimeoutDS18B20 = true;
}
