/*---------------------------------- Includes ----------------------------------------------*/

#include "prototypes.h"
#include "parameters.h"

/*--------------------------------- Globals ------------------------------------------------*/

volatile float fPWMClock = 0;
volatile float fNewFrequency[2] = { 0 };
volatile float fCurrentFrequency[2] = { 0 };

/*--------------------------------- Static -------------------------------------------------*/

static float fCurrentSetting = 0.0f;
static double fDifference = 0.0f;
static bool bPWMEnabled[2] = { true, true };

/*---------------------------------- Functions ---------------------------------------------*/

void SetPWMFrequency(float fNewSetting, eTunerType eTuner)
{
    fDifference = fabs((double) fNewSetting - (double) fCurrentFrequency[eTuner]);

    if (fNewSetting < PWM_RESOLUTION && bPWMEnabled[eTuner])
    {
        //Switch OFF PWM
        if (eTuner == Large) TimerDisable(WTIMER2_BASE, TIMER_B);
        else if (eTuner == Small) TimerDisable(WTIMER3_BASE, TIMER_B);
    }

    if (fDifference >= PWM_RESOLUTION)
    {
        //Ensure step timer is disabled
        if (eTuner == Large) TimerDisable(TIMER2_BASE, TIMER_BOTH);
        else if (eTuner == Small) TimerDisable(TIMER3_BASE, TIMER_BOTH);

        //Update and retrieve static variables
        fNewFrequency[eTuner] = fNewSetting;
        fCurrentSetting = fCurrentFrequency[eTuner];

        //Update PWM signal settings
        if ((fNewSetting <= fCurrentSetting * 2) || (fCurrentSetting == 0 && fNewSetting <= 30)) //SMALL CHANGES
        {
            if (fNewSetting >= PWM_RESOLUTION) //new setting at least 0.05Hz
            {
                //Update PWM frequency
                UpdatePWM(fNewSetting, eTuner);

                if (fCurrentSetting == 0)
                {
                    //Switch ON PWM
                    if (eTuner == Large) TimerEnable(WTIMER2_BASE, TIMER_B);
                    else if (eTuner == Small) TimerEnable(WTIMER3_BASE, TIMER_B);
                }

                fCurrentFrequency[eTuner] = fNewSetting;
            }
        }
        else //LARGE CHANGES
        {
            //Update static variables
            if (fCurrentSetting == 0) fCurrentFrequency[eTuner] = 30;
            else fCurrentFrequency[eTuner] *= 2;

            //Update PWM
            UpdatePWM(fCurrentFrequency[eTuner], eTuner);

            if (fCurrentSetting == 0)
            {
                //Switch ON PWM
                if (eTuner == Large) TimerEnable(WTIMER2_BASE, TIMER_B);
                else if (eTuner == Small) TimerEnable(WTIMER3_BASE, TIMER_B);
            }

            if (eTuner == Large)
            {
               //Start one-shot timer
               TimerLoadSet(TIMER2_BASE, TIMER_BOTH, (SysCtlClockGet() / fCurrentFrequency[0]) * STEP_NUMCYCLES);
               TimerEnable(TIMER2_BASE, TIMER_BOTH);
            }
            else if (eTuner == Small)
            {
               //Start one-shot timer
               TimerLoadSet(TIMER3_BASE, TIMER_BOTH, (SysCtlClockGet() / fCurrentFrequency[0]) * STEP_NUMCYCLES);
               TimerEnable(TIMER3_BASE, TIMER_BOTH);
            }
        }
    }
}

void UpdatePWM(float fNewSetting, eTunerType eTuner)
{
    uint32_t ulLoad = (uint32_t) round((SysCtlClockGet() - 1) / fNewSetting);

    if (eTuner == Large)
    {
        TimerLoadSet(WTIMER2_BASE, TIMER_B, ulLoad);
        TimerMatchSet(WTIMER2_BASE, TIMER_B, ulLoad * 0.5f);
        TimerEnable(WTIMER2_BASE, TIMER_B);
    }
    else if (eTuner == Small)
    {
        TimerLoadSet(WTIMER3_BASE, TIMER_B, ulLoad);
        TimerMatchSet(WTIMER3_BASE, TIMER_B, ulLoad * 0.5f);
        TimerEnable(WTIMER3_BASE, TIMER_B);
    }

    bPWMEnabled[eTuner] = true;
}

/*------------------------------ Interrupt Handlers ----------------------------------------*/

void IntHandlerTimerPWMLarge(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER2_BASE, true);
    TimerIntClear(TIMER2_BASE, ui32status);

    if (fNewFrequency[0] > fCurrentFrequency[0] * 2)
    {
        //Step frequency change
        fCurrentFrequency[0] *= 2;
        UpdatePWM(fCurrentFrequency[0], Large);

        //Restart one-shot timer
        TimerLoadSet(TIMER2_BASE, TIMER_BOTH, (SysCtlClockGet() / fCurrentFrequency[0]) * STEP_NUMCYCLES);
        TimerEnable(TIMER2_BASE, TIMER_BOTH);
    }
    else
    {
        //Final iteration
        fCurrentFrequency[0] = fNewFrequency[0];
        UpdatePWM(fCurrentFrequency[0], Large);
    }
}

void IntHandlerTimerPWMSmall(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER3_BASE, true);
    TimerIntClear(TIMER3_BASE, ui32status);

    if (fNewFrequency[1] > fCurrentFrequency[1] * 2)
    {
        //Step frequency change
        fCurrentFrequency[1] *= 2;
        UpdatePWM(fCurrentFrequency[1], Small);

        //Restart one-shot timer
        TimerLoadSet(TIMER3_BASE, TIMER_BOTH, (SysCtlClockGet() / fCurrentFrequency[1]) * STEP_NUMCYCLES);
        TimerEnable(TIMER3_BASE, TIMER_BOTH);
    }
    else
    {
        //Final iteration
        fCurrentFrequency[1] = fNewFrequency[1];
        UpdatePWM(fCurrentFrequency[1], Small);
    }
}

void IntHandlerTimerSpeedLarge(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER0_BASE, true);
    TimerIntClear(TIMER0_BASE, ui32status);
    bLargeSpeedTimerRunning = false;
}

void IntHandlerTimerSpeedSmall(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER5_BASE, true);
    TimerIntClear(TIMER5_BASE, ui32status);
    bSmallSpeedTimerRunning = false;
}

