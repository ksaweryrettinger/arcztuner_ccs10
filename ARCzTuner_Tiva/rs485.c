/*---------------------------------- Includes ----------------------------------------------*/

#include "prototypes.h"
#include "parameters.h"

/* --------------------------------- Globals -----------------------------------------------*/

//UART Communication
volatile uint8_t ucBufferUART[MAX_BYTES_UART_RX] = { 0 };
volatile uint8_t ucByteNumUART = 0;
volatile uint8_t ucTunerButtonsTemp = 0;
volatile bool bTunerRx = true;
volatile bool bDataRequest = false;

/*------------------------------ Interrupt Handlers ----------------------------------------*/

void IntHandlerRS485(void)
{
    uint8_t i;
    uint8_t ucByte;
    uint32_t ui32status;
    ui32status = UARTIntStatus(UART4_BASE, true);
    UARTIntClear(UART4_BASE, ui32status);

    //Receive interrupt
   if ((ui32status & UART_INT_RX) != 0u)
   {
       //Always fetch new byte
       ucByte = (uint8_t) UARTCharGet(UART4_BASE);

       //New request or data incoming - reset counter
       if (ucByte == TUNER_ID) ucByteNumUART = 0;

       //Store data and update counter
       ucBufferUART[ucByteNumUART] = ucByte;
       ucByteNumUART++;

        //Check buffer
        if (ucBufferUART[0] == TUNER_ID && ucByteNumUART == MAX_BYTES_UART_RX)
        {
            if (ucBufferUART[1] == '?') //data request
            {
                //Disable interrupts, switch to Tx mode
                IntDisable(INT_UART4);
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PIN_4);
                SysCtlDelay(10000);

                //Copy latest data
                if (!bCriticalSectionLocker)
                {
                    bCriticalSectionLocker = true;
                    for (i = 0; i < TUNER_NUMBYTES; i++) ucTunerDataLocal[i] = ucTunerDataMaster[i];
                    bCriticalSectionLocker = false;
                }

                //Send reply ('a' or 'b' followed by tuner data)
                for (i = 0; i < TUNER_NUMBYTES; i++) UARTCharPut(UART4_BASE, ucTunerDataLocal[i]);
                while (UARTBusy(UART4_BASE));

                //Switch to Rx mode, re-enable interrupts
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0);
                IntEnable(INT_UART4);
            }
            else if (ucBufferUART[1] & 0xC0) //new button configuration
            {
                //Disable interrupts, switch to Tx mode
                IntDisable(INT_UART4);
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PIN_4);
                SysCtlDelay(10000);

                //Copy new configuration
                bCriticalSectionLocker = true;
                ucTunerButtonsMaster = ucBufferUART[1];
                bCriticalSectionLocker = false;

                //Send reply
                UARTCharPut(UART4_BASE, ucTunerDataLocal[0]); //'a' or 'b'
                UARTCharPut(UART4_BASE, ucTunerButtonsMaster); //confirm configuration
                while (UARTBusy(UART4_BASE));

                //Switch to Rx mode, re-enable interrupts
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0);
                IntEnable(INT_UART4);
            }

           //Reset counter to prevent overflow
           ucByteNumUART = 0;
       }
   }
}
